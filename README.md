# k8s-workshop

Instalacion de azure cli on ubuntu Ref. https://learn.microsoft.com/en-us/cli/azure/install-azure-cli

Ejecutar en la terminal (linux)

```sh  
curl -sL https://aka.ms/InstallAzureCLIDeb | sudo bash
```
Para loguearno desde la terminal hacia el portal de azure ejecutamos lo siguiente:
```sh  
az login
```

A continuacion comandos para crear un cluster con un master node y un worker node:
```sh  
AKSRG=rsgrdemoeu202
AKSPOOLRG=rsgrakspooleu202
AKSNAME=aksdemoeu202


az group create --name $AKSRG --location eastus2
  
az aks create \
  --resource-group $AKSRG \
  --name $AKSNAME \
  --location eastus2 \
  --nodepool-name systempool \
  --node-count 1 \
  --node-vm-size "Standard_B2s" \
  --node-resource-group $AKSPOOLRG \
  --kubernetes-version 1.25.5 \
  --generate-ssh-keys

az aks nodepool add \
  --resource-group $AKSRG \
  --cluster-name $AKSNAME \
  --name userpool \
  --node-vm-size "Standard_B2s" \
  --enable-cluster-autoscaler \
  --node-count 1 \
  --min-count 1 \
  --max-count 1 \
  --mode user
```

Una vez finalizada en aprovisionamiento del AKS, procederemos a descargar el kubeconfig desde el portal y luego lo configuraremos en nuestra terminal local

```sh
export KUBECONFIG=$HOME/k8s-workshop/config
```
O solo ejecutamos el siguiente comando
```sh
az aks get-credentials --resource-group $AKSRG --name $AKSNAME -a
```

Instalacion de Kubectl ref: https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/
```sh
curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
```

Probamos la conexion obteniendo la informacion de los nodos:

```sh
kubectl get nodes -owide
```

# Pod
Archivos a usar:
- [1-pod.yaml](1-pod.yaml)

Comandos a ejecutar:

```sh
kubectl run demo-pod --image=gcr.io/google-samples/hello-app:1.0
kubectl port-forward pod/demo-pod 8181:8080
# Para probar desde consola
curl localhost:8181  


kubectl apply -f 1-pod.yaml  
kubectl describe po nginx
kubectl port-forward pod/nginx 8080:80
# Para probar desde consola
curl localhost:8080

kubectl exec -ti nginx -- sh
kubectl delete -f 1-pod.yaml
```
# ReplicaSet
Archivos a usar:
- [2-replicaset.yaml](2-replicaset.yaml)

Comandos a ejecutar:

```sh

kubectl apply -f 2-replicaset.yaml
kubectl get po,rs
kubectl describe rs demo-replicaset
kubectl describe rs/demo-replicaset
kubectl scale --replicas=10 rs demo-replicaset
```


# Deployment
Archivos a usar:
- [3-deployment.yaml](3-deployment.yaml)

Comandos a ejecutar:

```sh

kubectl create deployment  ghost --image=ghost:0.9 --replicas=5
kubectl delete deploy/ghost

kubectl create deployment  ghost --image=nginx:1.14.2 --replicas=5
kubectl set image deploy/ghost nginx=nginx:1.16.1
kubectl rollout status deploy/ghost
kubectl rollout history deploy/ghost
kubectl rollout undo deploy/ghost
kubectl rollout restart deploy/ghost
kubectl delete deploy/ghost

kubectl apply -f 3-deployment.yaml
kubectl scale deploy demo-deployment --replicas=20
kubectl delete <nombre-pod>
```
# DaemonSet
Archivos a usar:
- [4-daemonset.yaml](4-daemonset.yaml)

Comandos a ejecutar:

```sh

kubectl apply -f 4-daemonset.yaml
kubectl get nodes
kubectl get po -owide

# Incrementando el nodepool az command:
az aks nodepool scale --cluster-name $AKSNAME \
  --nodepool-name userpool \
  --resource-group $AKSRG \
  --node-count 1

# Volviendo a consultar despues de un tiempo
kubectl get nodes
kubectl get po -owide

```


# StatefulSet
Archivos a usar:
- [5_1-statefulset-pvc.yaml](5_1-statefulset-pvc.yaml)
- [5_2-statefulset.yaml](5_2-statefulset.yaml)

Comandos a ejecutar:
```sh

kubectl apply -f 5_1-statefulset-pvc.yaml
kubectl apply -f 5_2-statefulset.yaml 

kubectl get pvc
kubectl get sts
kubectl get po
kubectl describe po nombre_pod_pending

kubectl get pv,pvc,sts,po -owide

kubectl exec -it web-0 -- bash -c "echo \"<html><head></head><body><h1>Hello World v1 <h1></body></html>\" > /usr/share/nginx/html/index.html"
kubectl exec -it web-0 -- bash -c "curl localhost"
kubectl delete po web-0

kubectl exec -it web-0 -- bash -c "echo \"<html><head></head><body><h1>Hello World v2 <h1></body></html>\" > /usr/share/nginx/html/index.html"
kubectl exec -it web-0 -- bash -c "curl localhost"

# otra forma
kubectl port-forward pod/web-0 8282:80

# repetir pero borrando y creando nuevamente el statefulset
kubectl delete -f 5_2-statefulset.yaml 
kubectl delete -f 5_1-statefulset-pvc.yaml

```

# Services
Archivos a usar:
- [6_1-service-deployment.yaml](6_1-service-deployment.yaml)
- [6_2-service-clusterip.yaml](6_2-service-clusterip.yaml)
- [6_3-service-nodeport.yaml](6_3-service-nodeport.yaml)
- [6_4-service-loadbalancer.yaml](6_4-service-loadbalancer.yaml)



Comandos a usar:

```sh

kubectl apply -f 6_1-service-deployment.yaml
kubectl get po,rs,deploy
kubectl apply 6_2-service-clusterip.yaml

kubectl get svc
kubeclt describe svc hello-app-clusterip-service
kubectl get po -owide


kubectl run -i --tty --rm debug --image=curlimages/curl --restart=Never -- sh
# dentro ejecutamos lo siguiente
curl hello-app-clusterip-service
curl dato_cluster_ip_asignado

kubectl delete svc hello-app-clusterip-service

# NODEPORT

kubectl apply -f 6_3-service-nodeport.yaml
kubectl get svc
kubectl describe svc hello-app-nodeport-svc
#tomar nota del internal-ip
kubectl get nodes -owide

kubectl run -i --tty --rm debug --image=curlimages/curl --restart=Never -- sh
# dentro ejecutamos lo siguiente
curl internal_ip:30036

#en caso estemos conectados directamente al control plane no es necesario usar la imagen curl directamente ejecutamos
curl nombre_nodo:30036

kubectl delete svc hello-app-nodeport-svc
#LOADBALANCER

kubectl apply -f 6_4-service-loadbalancer.yaml
kubectl get svc

kubectl describe svc hello-app-loadbalancer-svc
# tomar nota del external-IP asigando
kubectl get svc

# ejecutar lo siguiente o abrir en el navegador
curl external_ip:8765 


```


# Ingress

Creando una IP Publica

```sh
az network public-ip create --resource-group $AKSPOOLRG --name AKSPublicIPForIngress --sku Standard --allocation-method static --query publicIp.ipAddress -o tsv
```

20.114.133.219
# Instalacion Helm

```sh
curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3
chmod 700 get_helm.sh
./get_helm.sh
```
```sh
helm version
# Create a namespace for your ingress resources
kubectl create namespace ingress-basic

# Add the official stable repository
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
#helm repo add stable https://kubernetes-charts.storage.googleapis.com/
helm repo update

#  Customizing the Chart Before Installing. 
helm show values ingress-nginx/ingress-nginx

# Use Helm to deploy an NGINX ingress controller
helm install ingress-nginx ingress-nginx/ingress-nginx \
    --namespace ingress-basic \
    --set controller.replicaCount=2 \
    --set controller.nodeSelector."kubernetes\.io/os"=linux \
    --set defaultBackend.nodeSelector."kubernetes\.io/os"=linux \
    --set controller.service.externalTrafficPolicy=Local \
    --set controller.service.loadBalancerIP="REPLACE_STATIC_IP"

# List Services with labels
kubectl get service -l app.kubernetes.io/name=ingress-nginx --namespace ingress-basic

# List Pods
kubectl get pods -n ingress-basic
kubectl get all -n ingress-basic


# Access Public IP
http://<Public-IP-created-for-Ingress>

```

Archivos a usar:
- [7_1-ingress-hello-v1-v2-deployment-svc.yaml](7_1-ingress-hello-v1-v2-deployment-svc.yaml)
- [7_2-hello-ingress.yaml](7_2-hello-ingress.yaml)

Comandos a ejecutar:

```sh
  
kubectl apply -f 7_1-7_1-ingress-hello-v1-v2-deployment-svc.yaml
kubectl apply -f 7_2-hello-ingress.yaml

kubectl get all
kubectl get ing 

# probando desde consola o abrir en navegador <Public-IP-created-for-Ingress>/v1    <Public-IP-created-for-Ingress>/v2
curl <Public-IP-created-for-Ingress>/v1
curl <Public-IP-created-for-Ingress>/v2

```

# HPA

Archivos a usar:
- [8_1-hpa-deploy-svc.yaml](8_1-hpa-deploy-svc.yaml)
- [8_2-hpa.yaml](8_2-hpa.yaml)

Comandos a ejecutar:

```sh
# Verificando que metric server is running
kubectl top pods -n kube-system
kubectl apply -f 8_1-hpa-deploy-svc.yaml
kubectl get po,deploy,svc
kubectl apply -f 8_2-hpa.yaml
kubectl get po,deploy,svc,hpa

# Iniciando la carga al servicio
kubectl run -i --tty load-generator --rm --image=busybox --restart=Never -- /bin/sh -c "while sleep 0.01; do wget -q -O- http://php-apache; done"

# Verificando como se comporta el hpa y los pods
kubectl get po,deploy,hpa
# Anclando fijamente
watch -n3 "kubectl get po,deploy,hpa"
```

# ConfigMap

Archivos a usar:
- [9_1-configmap.yaml](9_1-configmap.yaml)
- [9_2-configmap-pod.yaml](9_2-configmap-pod.yaml)

Comandos a Ejecutar:
```sh
kubectl apply -f 9_1-configmap.yaml
kubectl get cm
kubectl describe cm demo-configmap

kubectl apply -f 9_2-configmap-pod.yaml
kubectl get po
kubectl logs demo-pod-configmap

```

# Secrets
Archivos a usar:
- [10-secret-pod.yaml](10-secret-pod.yaml)

Comandos a Ejecutar:

```sh
echo -n 'admin' > username.txt
echo -n '1f2d1e2e67df' > password.txt
kubectl create secret generic db-credentials --from-file=user=username.txt --from-file=password=password.txt 

kubectl describe secret/db-credentials
kubectl get secret db-credentials -oyaml

kubectl apply -f 10-secret-pod.yaml
kubectl exec -it demo-use-secret -- sh
# Ejecutando dentro del pod
echo $SECRET_USERNAME
echo $SECRET_PASSWORD
```